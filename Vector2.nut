local vecUp = null
local vecRight = null

local vecDown = null
local vecLeft = null

local vecZero = null
local vecOne = null

// class

class Vector2
{
	static epsilon = 0.000001

	x = 0.0
	y = 0.0
	
	constructor (...)
	{
		switch (vargv.len())
		{
			// copy constructor
			
			case 1:
				if (!validArgument(vargv[0], Vector2, "table"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Vector2|table'"
			
				x = vargv[0].x
				y = vargv[0].y
			
				break
				
			// initial constructor
			
			case 2:
				if (!validArgument(vargv[0], "integer", "float"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[1], "integer", "float"))
					throw "parameter 2 has an invalid type '"+type(vargv[1])+"' ; expected: 'integer|float'"
					
				x = vargv[0].tofloat()
				y = vargv[1].tofloat()
			
				break
		}
	}
	
	// overloaded operators
	
	function _get(idx)
	{			
		switch (idx)
		{
			case 0: return x
			case 1: return y
			
			default: throw null
		}
	}
	
	function _set(idx, value)
	{			
		switch (idx)
		{
			case 0:
				x = value
				break
				
			case 1:
				y = value
				break
				
			default: 
				throw null
		}
	}
	
	function _add(arg)
	{
		local vec = Vector2(x, y)
	
		if (validArgument(arg, Vector2, "table"))
		{
			vec.x += arg.x
			vec.y += arg.y
		}
		else
		{
			vec.x += arg
			vec.y += arg
		}
		
		return vec
	}
	
	function _sub(arg)
	{		
		return this + -arg
	}
	
	function _unm()
	{		
		return Vector2(-x, -y)
	}
	
	function _mul(arg)
	{
		local vec = Vector2(x, y)
	
		if (validArgument(arg, Vector2, "table"))
		{
			vec.x *= arg.x
			vec.y *= arg.y
		}
		else
		{
			vec.x *= arg
			vec.y *= arg
		}
		
		return vec
	}
	
	function _div(arg)
	{
		if (validArgument(arg, Vector2, "table"))
			return this * Vector2(1.0 / arg.x, 1.0 / arg.y)
		else
			return this * (1.0 / arg)
	}
	
	// methods
	
	function add(arg)
	{
		if (validArgument(arg, Vector2, "table"))
		{
			x += arg.x
			y += arg.y
		}
		else
		{
			x += arg
			y += arg
		}
	}
	
	function sub(arg)
	{
		add(-arg)
	}
	
	function mul(arg)
	{
		if (validArgument(arg, Vector2, "table"))
		{
			x *= arg.x
			y *= arg.y
		}
		else
		{
			x *= arg
			y *= arg
		}
	}
	
	function div(arg)
	{
		if (validArgument(arg, Vector2, "table"))
		{
			x /= arg.x
			y /= arg.y
		}
		else
		{
			x /= arg
			y /= arg
		}
	}
	
	function set(x, y)
	{
		this.x = x
		this.y = y
	}
	
	function equals(vec)
	{
		if (!validArgument(vec, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector2|table'"
	
		local diff_x = x - vec.x
        local diff_y = y - vec.y

        local sqrMagnitude = diff_x * diff_x + diff_y * diff_y

        return sqrMagnitude < epsilon * epsilon
	}
	
	function sqrLength()
	{
		return (x * x + y * y)
	}
	
	function length()
	{
		return sqrt(sqrLength())
	}
	
	function sqrMagnitude()
	{
		return sqrLength()
	}
	
	function magnitude()
	{
		return length()
	}
	
	function normalized()
	{
		local vec = Vector2(this)
		local magnitude = magnitude()

		if (magnitude != 0)
			vec.mul(1.0 / magnitude)
		else
			vec.x = vec.y = 0
		
		return vec
	}
	
	function tostring(pattern = "Vector2(%f, %f)")
	{
		return format(pattern, x, y)
	}
	
	// static methods
	
	static function up() { return vecUp }
	static function right() { return vecRight }

	static function down() { return vecDown }
	static function left() { return vecLeft}

	static function zero() { return vecZero }
	static function one() { return vecOne }
	
	static function clampMagnitude(vec, maxLength, makeCopy = true)
	{
		if (!validArgument(vec, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector2|table'"
	
		if (!validArgument(maxLength, "integer", "float"))
			throw "parameter 2 has an invalid type '"+type(maxLength)+"' ; expected: 'integer|float'"
		
		local result = (makeCopy) ? Vector2(vec) : vec
		
		if (result.sqrMagnitude() > maxLength * maxLength)
		{
			normalize(result)
			result.mul(maxLength)
		}
		
		return result
	}
	
	static function min(lhs, rhs)
	{
		if (!validArgument(lhs, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector2|table'"
			
		if (!validArgument(rhs, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector2|table'"
		
		return Vector2(min(lhs.x, rhs.x), min(lhs.y, rhs.y))
	}
	
	static function max(lhs, rhs)
	{
		if (!validArgument(lhs, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector2|table'"
			
		if (!validArgument(rhs, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector2|table'"
			
		return Vector2(max(lhs.x, rhs.x), max(lhs.y, rhs.y))
	}
	
	static function distance(a, b)
	{
		if (!validArgument(a, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector2|table'"
			
		if (!validArgument(b, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector2|table'"
	
		return (a - b).magnitude()
	}
	
	static function scale(a, b)
	{
		if (!validArgument(a, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector2|table'"
			
		if (!validArgument(b, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector2|table'"
		
		return a * b
	}
	
	static function angle(from, to)
	{
		if (!validArgument(from, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(from)+"' ; expected: 'Vector2|table'"
			
		if (!validArgument(to, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(to)+"' ; expected: 'Vector2|table'"

		local cross = from.x * to.y + from.y * to.x
		return atan2(fabs(cross), dot(from, to)) * rad2Deg
	}
	
	static function signedAngle(from, to)
	{
		if (!validArgument(from, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(from)+"' ; expected: 'Vector2|table'"
			
		if (!validArgument(to, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(to)+"' ; expected: 'Vector2|table'"
		
		local cross = from.x * to.y + from.y * to.x
		return atan2(-cross, dot(from, to)) * rad2Deg
	}
	
	static function moveTowards(current, target, maxDistanceDelta)
	{
		if (!validArgument(current, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(current)+"' ; expected: 'Vector2|table'"
			
		if (!validArgument(current, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(target)+"' ; expected: 'Vector2|table'"

		if (!validArgument(maxDistanceDelta, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(maxDistanceDelta)+"' ; expected: 'integer|float'"
		
		local distance = target - current
		local magnitude = distance.magnitude()
		
		if (magnitude <= maxDistanceDelta || magnitude == 0)
			return target
		
		return current + (distance / magnitude) * maxDistanceDelta
	}
	
	static function normalize(vec)
	{
		if (!validArgument(vec, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector2|table'"
	
		local magnitude = vec.magnitude()

		if (magnitude != 0)
			vec.mul(1.0 / magnitude)
		else
			vec.x = vec.y = 0
			
		return vec
	}
	
	static function dot(lhs, rhs)
	{
		if (!validArgument(lhs, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(lhs)+"' ; expected: 'Vector2|table'"
			
		if (!validArgument(rhs, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(rhs)+"' ; expected: 'Vector2|table'"
		
		return lhs.x * rhs.x + lhs.y * rhs.y
	}

	static function perpendicular(inDirection)
	{
		if (!validArgument(inDirection, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(inDirection)+"' ; expected: 'Vector2|table'"
	
		return Vector2(-inDirection.y, inDirection.x)
	}
	
	static function reflect(inDirection, inNormal)
	{
		if (!validArgument(inDirection, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(inDirection)+"' ; expected: 'Vector2|table'"
		
		if (!validArgument(inNormal, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(inNormal)+"' ; expected: 'Vector2|table'"
		
		return inDirection + inNormal * -2 * dot(inDirection, inNormal)
	}
	
	static function lerpUnclamped(a, b, perc)
	{
		if (!validArgument(a, Vector2, "table"))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Vector2|table'"
	
		if (!validArgument(b, Vector2, "table"))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Vector2|table'"
	
		if (!validArgument(perc, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(perc)+"' ; expected: 'integer|float'"
	
		return Vector2(
			a.x + ((b.x - a.x) * perc), 
			a.y + ((b.y - a.y) * perc))
	}
	
	static function lerp(a, b, perc)
	{
		return lerpUnclamped(a, b, clamp(perc, 0, 1))
	}
}

// creating shorthand vector objects

vecUp = Vector2(0, 1)
vecRight = Vector2(1, 0)

vecDown = Vector2(0, -1)
vecLeft = Vector2(-1, 0)

vecZero = Vector2(0, 0)
vecOne = Vector2(1, 1)
