local quatIdentity = null

class Quaternion
{
	static epsilon = 0.000001

	x = 0.0
	y = 0.0
	z = 0.0
	w = 1.0

	constructor(...)
	{	
		switch (vargv.len())
		{
			// copy constructor
			
			case 1:
				if (!(vargv[0] instanceof Quaternion))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Quaternion'"
			
				x = vargv[0].x
				y = vargv[0].y
				z = vargv[0].z
				w = vargv[0].w
			
				break
			
			// initial constructor
			
			case 2:
				if (!validArgument(vargv[0], Vector3, "table"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'Vector3|table'"
			
				if (!validArgument(vargv[1], "integer", "float"))
					throw "parameter 2 has an invalid type '"+type(vargv[1])+"' ; expected: 'integer|float'"
				
				x = vargv[0].x
				y = vargv[0].y
				z = vargv[0].z
				w = vargv[1].tofloat()
				
				break
				
			// initial constructor
				
			case 4:
				if (!validArgument(vargv[0], "integer", "float"))
					throw "parameter 1 has an invalid type '"+type(vargv[0])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[1], "integer", "float"))
					throw "parameter 2 has an invalid type '"+type(vargv[1])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[2], "integer", "float"))
					throw "parameter 3 has an invalid type '"+type(vargv[2])+"' ; expected: 'integer|float'"
					
				if (!validArgument(vargv[3], "integer", "float"))
					throw "parameter 3 has an invalid type '"+type(vargv[3])+"' ; expected: 'integer|float'"
			
				x = vargv[0].tofloat()
				y = vargv[1].tofloat()
				z = vargv[2].tofloat()
				w = vargv[3].tofloat()
			
				break
		}
	}
	
	// overloaded operators
	
	function _get(idx)
	{			
		switch (idx)
		{
			case 0: return x
			case 1: return y
			case 2: return z
			case 3: return w
			
			default: throw null
		}
	}
	
	function _set(idx, value)
	{
		switch (idx)
		{
			case 0:
				x = value
				break
				
			case 1:
				y = value
				break
				
			case 2:
				z = value
				break
				
			case 3:
				w = value
			
			default: 
				throw null
		}
	}
	
	function _mul(arg)
	{
		if (arg instanceof Quaternion)
		{
			local q = Quaternion(x, y, z, w)
		
			local x = q.w * arg.x + q.x * arg.w + q.y * arg.z - q.z * arg.y
			local y = q.w * arg.y + q.y * arg.w + q.z * arg.x - q.x * arg.z
			local z = q.w * arg.z + q.z * arg.w + q.x * arg.y - q.y * arg.x
			local w = q.w * arg.w - q.x * arg.x - q.y * arg.y - q.z * arg.z
		
			q.x = x
			q.y = y
			q.z = z
			q.w = w
		
			return q
		}
		else if (validArgument(arg, Vector3, "table"))
		{
			local qx2 = x * 2
			local qy2 = y * 2
			local qz2 = z * 2
			local qx_qx2 = x * qx2
			local qy_qy2 = y * qy2
			local qz_qz2 = z * qz2
			local qx_qy2 = x * qy2
			local qx_qz2 = x * qz2
			local qy_qz2 = y * qz2
			local qw_qx2 = w * qx2
			local qw_qy2 = w * qy2
			local qw_qz2 = w * qz2

			local result = Vector3()

			result.x = (1 - (qy_qy2 + qz_qz2)) * arg.x + (qx_qy2 - qw_qz2) * arg.y + (qx_qz2 + qw_qy2) * arg.z
			result.y = (qx_qy2 + qw_qz2) * arg.x + (1 - (qx_qx2 + qz_qz2)) * arg.y + (qy_qz2 - qw_qx2) * arg.z
			result.z = (qx_qz2 - qw_qy2) * arg.x + (qy_qz2 + qw_qx2) * arg.y + (1 - (qx_qx2 + qy_qy2)) * arg.z
			
			return result
		}
		else if (type(arg) == "integer" || type(arg) == "float")
		{
			local q = Quaternion(x, y, z, w)

			q.x *= arg
			q.y *= arg
			q.z *= arg
			q.w *= arg

			return q
		}
	}
	
	// methods
	
	function mul(arg)
	{
		if (arg instanceof Quaternion)
		{
			local x = this.w * arg.x + this.x * arg.w + this.y * arg.z - this.z * arg.y
			local y = this.w * arg.y + this.y * arg.w + this.z * arg.x - this.x * arg.z
			local z = this.w * arg.z + this.z * arg.w + this.x * arg.y - this.y * arg.x
			local w = this.w * arg.w - this.x * arg.x - this.y * arg.y - this.z * arg.z
		
			this.x = x
			this.y = y
			this.z = z
			this.w = w
		}
		else if (type(arg) == "integer" || type(arg) == "float")
		{
			this.x *= arg
			this.y *= arg
			this.z *= arg
			this.w *= arg
		}
	}
	
	function set(x, y, z, w)
	{
		this.x = x
		this.y = y
		this.z = z
		this.w = w
	}
	
	function equals(q)
	{
		if (!(q instanceof Quaternion))
			throw "parameter 1 has an invalid type '"+type(q)+"' ; expected: 'Quaternion'"
	
		local diff_x = x - q.x
        local diff_y = y - q.y
        local diff_z = z - q.z
		local diff_w = w - q.w

        local sqrMagnitude = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z

        return sqrMagnitude < epsilon * epsilon
	}
	
	function normalized()
	{
		local magnitude = dot(this, this)

		if (magnitude == 1)
			return this

		local q = Quaternion(this)
		local invMagnitude = 1.0 / sqrt(magnitude)

		if (invMagnitude != 0)
			q.mul(invMagnitude)
		
		return q
	}
	
	function toAngleAxis(angle, axis)
	{
		if (!validArgument(angle, "integer", "float"))
			throw "parameter 1 has an invalid type '"+type(angle)+"' ; expected: 'integer|float'"
	
		if (!validArgument(axis, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(axis)+"' ; expected: 'Vector3|table'"
	
		angle = acos(w) * rad2Deg * 2
		local div = sqrt(1 - w * w)
		
		if (div < epsilon)
		{
			axis.set(1, 0, 0)
			return angle
		}
		
		axis.set(x / div, y / div, z / div)
		
		return angle
	}

	function setLookRotation(forward, up = Vector3.up())
	{
		if (!validArgument(forward, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(forward)+"' ; expected: 'Vector3|table'"

		if (!validArgument(up, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(up)+"' ; expected: 'Vector3|table'"

		Vector3.orthoNormalize(forward, up)
		local right = Vector3.cross(up, forward)

		local w4_recip = 1.0 / (4.0 * w)

		x = (up.z - forward.y) * w4_recip
		y = (forward.x - right.z) * w4_recip
		z = (right.y - up.x) * w4_recip
		w = sqrt(1.0 + right.x + up.y + forward.z) * 0.5
	}
	
	function eulerAngles()
	{
		local sqx = x*x
		local sqy = y*y
		local sqz = z*z
		local sqw = w*w
		local unit = sqx + sqy + sqz + sqw
		local test = x*y + z*w
		
		local euler = Vector3(0, 0, 0)
		
		if (test > 0.5 * unit)
		{ // singularity at north pole
			euler.x = 2 * atan2(x, w)
			euler.y = 180
		}
		else if (test < -0.5 * unit)
		{ // singularity at south pole
			euler.x = -2 * atan2(x, w)
			euler.y = -180
		}
		else
		{
			euler.x = atan2(2 * y * w - 2 * x * z , sqx - sqy - sqz + sqw)
			euler.y = asin(2 * test / unit)
			euler.z = atan2(2 * x * w - 2 * y * z , -sqx + sqy - sqz + sqw)
		}

		euler.mul(rad2Deg)
		
		if (euler.x < 0)
			euler.x += 360
		else if (euler.x > 360)
			euler.x -= 360

		if (euler.y < 0)
			euler.y += 360
		else if (euler.y > 360)
			euler.y -= 360

		if (euler.z < 0)
			euler.z += 360
		else if (euler.z > 360)
			euler.z -= 360
		
		return euler
	}

	function tostring(pattern = "Quaternion(%f, %f, %f, %f)")
	{
		return format(pattern, x, y, z, w)
	}
	
	// static methods
	
	static function identity() { return quatIdentity }
	
	static function dot(a, b)
	{
		if (!(a instanceof Quaternion))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Quaternion'"
		
		if (!(b instanceof Quaternion))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Quaternion'"
	
		return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w
	}
	
	static function normalize(q)
	{
		if (!(q instanceof Quaternion))
			throw "parameter 1 has an invalid type '"+type(q)+"' ; expected: 'Quaternion'"
	
		local magnitude = dot(q, q)

		if (magnitude == 1)
			return q
		
		local invMagnitude = 1.0 / sqrt(magnitude)

		if (invMagnitude != 0)
			q.mul(invMagnitude)
		
		return q
	}
	
	static function inverse(q)
	{
		return Quaternion(-q.x , -q.y, -q.z, q.w)
	}
	
	static function euler(...)
	{
		switch (vargv.len())
		{
			case 1:
				local vec = vargv[0]
				
				if (!validArgument(vec, Vector3, "table"))
					throw "parameter 1 has an invalid type '"+type(vec)+"' ; expected: 'Vector3|table'"
					
				local pitch = vec.x * deg2Rad * 0.5
				local yaw = vec.y * deg2Rad * 0.5
				local roll = vec.z * deg2Rad * 0.5
				
				local q = Quaternion()

				local sp = sin(pitch)
				local cp = cos(pitch)
				local sy = sin(yaw)
				local cy = cos(yaw)
				local sr = sin(roll)
				local cr = cos(roll)
				
				q.x = sp * cy * cr - cp * sy * sr
				q.y = cp * sy * cr + sp * cy * sr
				q.z = cp * cy * sr - sp * sy * cr
				q.w = sp * sy * sr + cp * cy * cr
		
				return q
		
			case 3:
				local pitch = vargv[0] * deg2Rad * 0.5
				local yaw = vargv[1] * deg2Rad * 0.5
				local roll = vargv[2] * deg2Rad * 0.5
				
				if (type(pitch) != "integer" && type(pitch) != "float")
					throw "parameter 1 has an invalid type '"+type(pitch)+"' ; expected: 'integer|float'"
					
				if (type(yaw) != "integer" && type(yaw) != "float")
					throw "parameter 2 has an invalid type '"+type(yaw)+"' ; expected: 'integer|float'"
					
				if (type(roll) != "integer" && type(roll) != "float")
					throw "parameter 3 has an invalid type '"+type(roll)+"' ; expected: 'integer|float'"
			
				local q = Quaternion()

				local sp = sin(pitch)
				local cp = cos(pitch)
				local sy = sin(yaw)
				local cy = cos(yaw)
				local sr = sin(roll)
				local cr = cos(roll)
				
				q.x = sp * cy * cr - cp * sy * sr
				q.y = cp * sy * cr + sp * cy * sr
				q.z = cp * cy * sr - sp * sy * cr
				q.w = sp * sy * sr + cp * cy * cr
				
				return q
		}
	}
	
	static function angle(a, b)
	{
		local dot = dot(a, b)
	
		return acos(min(fabs(dot), 1.0)) * 2.0 * rad2Deg
	}
	
	static function angleAxis(angle, axis)
	{
		if (type(angle) != "integer" && type(angle) != "float")
			throw "parameter 1 has an invalid type '"+type(angle)+"' ; expected: 'integer|float'"
	
		if (!validArgument(axis, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(axis)+"' ; expected: 'Vector3|table'"
	
		angle *= deg2Rad * 0.5
	
		local normAxis = axis.normalized()
		local s = sin(angle) 
    
		local x = normAxis.x * s
		local y = normAxis.y * s
		local z = normAxis.z * s
		local w = cos(angle)
		
		return Quaternion(x, y, z, w)
	}

	static function lookRotation(forward, up = Vector3.up())
	{
		// https://answers.unity.com/questions/819699/calculate-quaternionlookrotation-manually.html?fbclid=IwAR2CR8JyIYPKN8_qR4i5QkXeb-AvMreUFx26w-S4_7ILQnIn_jpqvGjY488

		if (!validArgument(forward, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(forward)+"' ; expected: 'Vector3|table'"

		if (!validArgument(up, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(up)+"' ; expected: 'Vector3|table'"

		if (forward.equals(Vector3.zero()))
			return Quaternion.identity()
		
		if (Vector3.cross(forward, up).equals(Vector3.zero()))
			return fromToRotation(Vector3.forward(), forward)

		up = up.normalized()
		
		local v =  forward + up * -Vector3.dot(up, forward)
		local q = fromToRotation(Vector3.forward(), v)

		return fromToRotation(v, forward) * q
	}

	static function fromToRotation(u, v)
	{
		// https://stackoverflow.com/questions/1171849/finding-quaternion-representing-the-rotation-from-one-vector-to-another

		if (!validArgument(u, Vector3, "table"))
			throw "parameter 1 has an invalid type '"+type(u)+"' ; expected: 'Vector3|table'"

		if (!validArgument(v, Vector3, "table"))
			throw "parameter 2 has an invalid type '"+type(v)+"' ; expected: 'Vector3|table'"

		u = u.normalized()
		v = v.normalized()

		local k_cos_theta = Vector3.dot(u, v)
		local k = sqrt(u.magnitude() * v.magnitude())

		local vec

		if (k_cos_theta / k == -1)
		{
			local x = fabs(u.x)
			local y = fabs(u.y)
			local z = fabs(u.z)

			local other
			
			if (x < y)
			{
				if (x < z)
					other = Vector3.right()
				else
					other = Vector3.forward()
			}
			else
			{
				if (y < z)
					other = Vector3.up()
				else
					other = Vector3.forward()
			}

			vec = Vector3.cross(v, other)
			return Quaternion(fabs(vec.x), fabs(vec.y), fabs(vec.z), 0)
		}

		vec = Vector3.cross(u, v)

		return normalize(Quaternion(vec.x, vec.y, vec.z, k_cos_theta + k))
	}
	
	static function lerpUnclamped(a, b, perc)
	{
		if (!(a instanceof Quaternion))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Quaternion'"
			
		if (!(b instanceof Quaternion))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Quaternion'"
	
		if (!validArgument(perc, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(perc)+"' ; expected: 'integer|float'"
	
		local sign = sign(Quaternion.dot(a, b))
		
		local x = a.x + perc * (b.x * sign - a.x)
		local y = a.y + perc * (b.y * sign - a.y)
		local z = a.z + perc * (b.z * sign - a.z)
		local w = a.w + perc * (b.w * sign - a.w)
		
		local q = Quaternion(x, y, z, w)
		
		return normalize(q)
	}
	
	static function lerp(a, b, perc)
	{
		return lerpUnclamped(a, b, clamp(perc, 0, 1))
	}

	static function slerpUnclamped(a, b, perc)
	{
		if (!(a instanceof Quaternion))
			throw "parameter 1 has an invalid type '"+type(a)+"' ; expected: 'Quaternion'"
			
		if (!(b instanceof Quaternion))
			throw "parameter 2 has an invalid type '"+type(b)+"' ; expected: 'Quaternion'"
	
		if (validArgument(perc, "integer", "float"))
			throw "parameter 3 has an invalid type '"+type(perc)+"' ; expected: 'integer|float'"
	
		local p1 = 1, p2 = 0
		
		local dot = dot(a, b)
		dot = clamp(dot, -1, 1)
		
		local alpha = acos(dot)
		local sinAlpha = sin(alpha)

		if (sinAlpha > epsilon)
		{
			p1 = cos(perc * alpha) / sinAlpha
			p2 = sin(perc * alpha) / sinAlpha
		}

		local x = p1 * a.x + p2 * b.x
		local y = p1 * a.y + p2 * b.y
		local z = p1 * a.z + p2 * b.z
		local w = p1 * a.w + p2 * b.w
		
		local q = Quaternion(x, y, z, w)
		
		return normalize(q)
	}
	
	static function slerp(a, b, perc)
	{
		return slerpUnclamped(a, b, clamp(perc, 0, 1))
	}
	
	static function rotateTowards(from, to, maxDegreesDelta)
	{
		local angle = angle(from, to)
		
		if (angle == 0)
			return to
		
		return slerp(from, to, maxDegreesDelta / angle)
	}
}

// creating shorthand quaternion objects

quatIdentity = Quaternion(0, 0, 0, 1)